package com.hoanghung.e_learning.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.home.HomeActivity;
import com.hoanghung.e_learning.register.Register;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends Activity {

    private Button btnLogin;
    private ImageView btnShowPassWord;
    private EditText editTextPassword;
    private EditText editTextUsername;
    private Button buttonRegister;
    private ProgressBar loading_login;
    private String URL = "";
    RelativeLayout relativeLayout;
    AccountManager accountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        mapping();
        setOnClick();
    }

    private void setOnClick() {

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                View view = getCurrentFocus();
                if (view == null) {
                    view = new View(getApplicationContext());
                }
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mEmail = editTextUsername.getText().toString();
                String mPass = editTextPassword.getText().toString();
                if (!mEmail.isEmpty() && !mPass.isEmpty()) {
                    login(mEmail, mPass);
                } else {
//                    mEmail = "quy151298@gmail.com";
//                    mPass = "123456789Aa!";
//                    editTextUsername.setText(mEmail);
//                    editTextPassword.setText(mPass);
//                    login(mEmail, mPass);
                    Toast.makeText(getApplicationContext(), "Hãy nhập đủ thông tin", Toast.LENGTH_SHORT).show();
                } }
        });

        btnShowPassWord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    editTextPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    editTextPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_NORMAL);
                    return true;
                }
                return false;
            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
            }
        });
    }

    private void mapping() {

        overridePendingTransition(R.anim.move_in_left, R.anim.move_out_left);
        accountManager = new AccountManager(Login.this);

        relativeLayout = (RelativeLayout) findViewById(R.id.layout);
        btnLogin = (Button) findViewById(R.id.BtnLogin);
        btnShowPassWord = (ImageView) findViewById(R.id.button_ic_eye);
        editTextPassword = (EditText) findViewById(R.id.edit_text_password);
        editTextUsername = (EditText) findViewById(R.id.edit_text_usename);
        buttonRegister = (Button) findViewById(R.id.button_registration);
        loading_login = (ProgressBar) findViewById(R.id.loading_login);
        loading_login.setVisibility(View.GONE);
    }

    private void login(final String email, final String password) {
        loading_login.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnLogin.setText(null);
        loading_login = (ProgressBar) findViewById(R.id.loading_login);
        btnLogin = (Button) findViewById(R.id.BtnLogin);
        Intent intent = new Intent(Login.this, HomeActivity.class);

        URL = getResources().getString(R.string.sever) + "login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                loading_login.setVisibility(View.GONE);
                btnLogin.setText("Login");
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status_code").equals("200")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        JSONObject jsonObjectArray = jsonArray.getJSONObject(0);
                        String userId = jsonObjectArray.getString("id");
                        String userName = jsonObjectArray.getString("name");
                        String userEmail = jsonObjectArray.getString("email");

                        accountManager.setHasLogin(true);
                        accountManager.setUserId(userId);
                        accountManager.setUserName(userName);
                        accountManager.setUserEmail(userEmail);

                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        startActivity(intent);
                        btnLogin.setEnabled(true);
                        finish();
                    } else if (jsonObject.getString("status_code").equals("401")) {
                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Catch", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Sai tài khoản hoặc mật khẩu", Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    Toast.makeText(getApplicationContext(), "Thời gian kết nối chậm, vui lòng kiểm tra đường truyền", Toast.LENGTH_LONG).show();
                }else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(), "Lỗi máy chủ, vui lòng kiểm tra đường truyền", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getApplicationContext(), "Lỗi kết nối, vui lòng kiểm tra đường truyền", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(), "Lỗi không xác định, vui lòng kiểm tra đường truyền", Toast.LENGTH_LONG).show();
                }

                loading_login.setVisibility(View.GONE);
                btnLogin.setText("Login");
                btnLogin.setEnabled(true);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
