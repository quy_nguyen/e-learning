package com.hoanghung.e_learning.login;

import android.os.Bundle;
import android.view.WindowManager;

import com.hoanghung.e_learning.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class WellcomeActivity  extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
