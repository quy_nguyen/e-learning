package com.hoanghung.e_learning.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.home.HomeActivity;
import com.hoanghung.e_learning.login.Login;
import com.hoanghung.e_learning.login.WellcomeActivity;
import com.hoanghung.e_learning.model.AccountManager;

import androidx.appcompat.app.AppCompatActivity;
import pl.droidsonroids.gif.GifImageView;

public class SplashActivity extends AppCompatActivity {

    AccountManager accountManager;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        try {
            synchronized (this) {
                wait(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_splash);
        accountManager = new AccountManager(this);
        GifImageView gifImageView = (GifImageView) findViewById(R.id.gif);
        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if (accountManager.getHasLogin()) {
                    intent = new Intent(SplashActivity.this, HomeActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, WellcomeActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }.start();

    }

}
