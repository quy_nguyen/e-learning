package com.hoanghung.e_learning.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.homecontent.HomeContentActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.RecyclerViewHolder>{
    Context context;
    int resource;
    List<ContentCourse> arrayList;

    public HomeAdapter(Context context, int resource,  List<ContentCourse> arrayList){
        this.context = context;
        this.resource = resource;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(resource, parent, false);
        return new HomeAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapter.RecyclerViewHolder holder, int position) {
        holder.textView.setText(arrayList.get(position).getNamecourse());
        if(arrayList.get(position).getId() == 2 | arrayList.get(position).getId() == 1){
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle setBundle = new Bundle();
                    setBundle.putString("title", arrayList.get(position).getNamecourse());
                    setBundle.putInt("id", arrayList.get(position).getId());
                    Intent intent = new Intent(context, HomeContentActivity.class);
                    intent.putExtra("bundle", setBundle);
                    context.startActivity(intent);
                }
            });
        } else {
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Chức năng đang phát triển", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.name);
        }
    }
}
