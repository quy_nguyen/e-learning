package com.hoanghung.e_learning.home;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.login.Login;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;
import com.hoanghung.e_learning.model.APIService;

import java.time.Instant;
import java.util.List;
import java.util.Random;
import java.util.Timer;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    AccountManager accountManager;
    RecyclerView listView;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("onCreate", "onCreate");
        setContentView(R.layout.activity_home);
        fillData();
        setCustomActionBar();
        setCustomDialog();
        swipeRefresh();
//        checktimein();
    }

//    private void furi() {
//        FuriganaView furiganaView = (FuriganaView) findViewById(R.id.furigana_view);
//        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                if(status != TextToSpeech.ERROR) {
//                    t1.setLanguage(Locale.JAPAN);
//                }
//            }
//        });
//
//        furiganaView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String toSpeak = furiganaView.getText().toString();
//                Toast.makeText(getApplicationContext(), toSpeak,Toast.LENGTH_SHORT).show();
//                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
//            }
//        });
//    }

    private void swipeRefresh() {
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fillData();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 5000);
            }
        });

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(this, R.color.holo_blue_bright),
                ContextCompat.getColor(this, R.color.holo_green_light),
                ContextCompat.getColor(this, R.color.holo_orange_light),
                ContextCompat.getColor(this, R.color.holo_red_light)
        );
    }

    private void fillData() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        accountManager = new AccountManager(HomeActivity.this);

        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<List<ContentCourse>> call = service.getContestCourse();
        call.enqueue(new Callback<List<ContentCourse>>() {
            @Override
            public void onResponse(Call<List<ContentCourse>> call, retrofit2.Response<List<ContentCourse>> response) {
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                listView = (RecyclerView) findViewById(R.id.list_home);
                HomeAdapter homeAdapter = new HomeAdapter(HomeActivity.this, R.layout.design_category, response.body());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this, RecyclerView.VERTICAL, false);
                listView.setHasFixedSize(true);
                listView.setLayoutManager((linearLayoutManager));
                listView.setAdapter(homeAdapter);

            }

            @Override
            public void onFailure(Call<List<ContentCourse>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Kiểm tra đường truyền", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void checktimein() {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 9) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String codeauto = salt.toString();
        Log.e("codeauto", codeauto);
        accountManager.setCodeAuto(codeauto);

        long unixTime = System.currentTimeMillis() / 1000L;
        Log.e("unixTime", unixTime+"");

        APIService apiService = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject> call = apiService.checkTimeIn(unixTime, accountManager.getUserId(), codeauto);

        call.enqueue(new Callback<APIObject>() {
            @Override
            public void onResponse(Call<APIObject> call, Response<APIObject> response) {
                Log.e("onResponse", "onResponse");
                CountDownTimer countDownTimer = new CountDownTimer(100000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        Log.e("onTick", "onTick");
                    }

                    public void onFinish() {
                        checktimeout();
                    }
                };
                countDownTimer.start();
            }

            @Override
            public void onFailure(Call<APIObject> call, Throwable t) {
                Log.e("onResponse", "t");
            }
        });
    }

    private void checktimeout() {
        long unixTime = System.currentTimeMillis() / 1000L;
        Log.e("unixTime", unixTime+"");

        APIService apiService = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject> call = apiService.checkTimeIn(unixTime, accountManager.getUserId(), accountManager.getCodeAuto());

        call.enqueue(new Callback<APIObject>() {
            @Override
            public void onResponse(Call<APIObject> call, Response<APIObject> response) {
                CountDownTimer countDownTimer = new CountDownTimer(100000, 1000) {

                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        checktimeout();
                    }
                };
                countDownTimer.start();
            }

            @Override
            public void onFailure(Call<APIObject> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem name = menu.findItem(R.id.item_user_name);
        name.setTitle(accountManager.getUserName());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.item_user_name:
                Context con;
                try {
                    con = createPackageContext("com.example.hackerman", 0);
                    SharedPreferences pref = con.getSharedPreferences(
                            "demopref", Context.MODE_PRIVATE);
                    String data = pref.getString("demostring", "No Value");
                    Log.e("Data from", data);
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e("Not data shared", e.toString());
                }
                return true;

            case R.id.item_cam_nhan:
                Toast.makeText(getApplicationContext(), "Chức năng đang được phát triển", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.item_logout:
                accountManager.logout();
                Intent intent = new Intent(HomeActivity.this, Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setCustomDialog() {
        if (accountManager.getHasPopup() == false) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder
                    .setCancelable(false)
                    .setMessage("Bạn nên kiểm tra đầu vào trước khi bắt đầu học")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    accountManager.setHasPopup(true);
                                    dialog.cancel();
                                }
                            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    private void setCustomActionBar() {
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar_home);
        getSupportActionBar().setElevation(0);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Bạn có chắc muốn thoát khỏi ứng dụng ?");
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
//                                Android.os.Process.killProcess(Android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
