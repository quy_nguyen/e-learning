package com.hoanghung.e_learning.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentCourse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("namecourse")
    @Expose
    private String namecourse;

    public ContentCourse(Integer id, String namecourse) {
        this.id = id;
        this.namecourse = namecourse;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNamecourse() {
        return namecourse;
    }

    public void setNamecourse(String namecourse) {
        this.namecourse = namecourse;
    }
}
