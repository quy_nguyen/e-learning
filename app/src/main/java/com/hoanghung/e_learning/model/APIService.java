package com.hoanghung.e_learning.model;

import com.hoanghung.e_learning.home.ContentCourse;
import com.hoanghung.e_learning.homecontent.LevelContent;
import com.hoanghung.e_learning.homecontent.LevelStatus;
import com.hoanghung.e_learning.lesson.ContentData;
import com.hoanghung.e_learning.lesson.VideoDoc;
import com.hoanghung.e_learning.lessoncontent.TypeOfTest;
import com.hoanghung.e_learning.listlesson.CourseContent;
import com.hoanghung.e_learning.result.Submitanswer;
import com.hoanghung.e_learning.test.Question;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIService {
    @GET("getcontestcourse")
    Call<List<ContentCourse>> getContestCourse();

    @POST("getlevelcontent")
    @FormUrlEncoded
    Call<APIObject<LevelContent>> getLevelContent(@Field("id") int id);

    @POST("levelstatus")
    @FormUrlEncoded
    Call<APIObject<LevelStatus>> getLevelStatus(@Field("userid") String id);

    @POST("postStatus")
    @FormUrlEncoded
    Call<APIObject> postStatus(@Field("userid") String userid,
                       @Field("lvid") int lvid,
                       @Field("status") int status);

    @POST("getcoursecontent")
    @FormUrlEncoded
    Call<APIObject<CourseContent>> getCourseContent(@Field("id") int id);

    @POST("contentdata")
    @FormUrlEncoded
    Call<APIObject<ContentData>> getContentData (@Field("id") int id);

    @POST("getvideodoc")
    @FormUrlEncoded
    Call<APIObject<VideoDoc>> getVideoDoc (@Field("id") int id);

    @POST("gettypeoftest")
    @FormUrlEncoded
    Call<APIObject<TypeOfTest>> getTypeOfTest (@Field("id") int id);

    @POST("getquestion")
    @FormUrlEncoded
    Call<APIObject<Question>> getQuestion (@Field("id") int id);

    @POST("checktimein")
    @FormUrlEncoded
    Call<APIObject> checkTimeIn (@Field("timein") long timein,
                                           @Field("userid") String userid,
                                           @Field("codeauto") String codeauto);

    @POST("checktimeout")
    @FormUrlEncoded
    Call<APIObject> checkTimeOut (@Field("timeout") long timeout,
                                 @Field("userid") String userid,
                                 @Field("codeauto") String codeauto);

    @POST("submitanswer")
    @FormUrlEncoded
    Call<APIObject<Submitanswer>> submitAnswer (@Field("userid") String userid,
                                                @Field("contentdataid") int contentdata,
                                                @Field("id") int id,
                                                @Field("idanswer[]") ArrayList<Integer> idanswer,
                                                @Field("answer[]") ArrayList<String> answer);

}
