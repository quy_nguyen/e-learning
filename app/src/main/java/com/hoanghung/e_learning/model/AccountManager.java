package com.hoanghung.e_learning.model;

import android.content.Context;
import android.content.SharedPreferences;

public class AccountManager {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    private static final String HAS_LOGIN = "HAS_LOGIN";
    private static final String HAS_POPUP = "HAS_POPUP";

    public AccountManager(Context context){
        this.context = context;
        preferences = context.getSharedPreferences("User", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public boolean getHasLogin(){
        return preferences.getBoolean(HAS_LOGIN, false);
    }

    public void setHasLogin(boolean hasLogin){
        editor.putBoolean(HAS_LOGIN, hasLogin);
        editor.commit();
    }

    public boolean getHasPopup(){
        return preferences.getBoolean(HAS_POPUP, false);
    }

    public void setHasPopup(boolean hasPopup){
        editor.putBoolean(HAS_POPUP, hasPopup);
        editor.commit();
    }

    public void logout(){
        editor.putBoolean(HAS_LOGIN, false);
        editor.putBoolean(HAS_POPUP, false);
        editor.putString("userId", null);
        editor.putString("userName", null);
        editor.putString("userEmail", null);
        editor.commit();
    }

    public String getUserId() {
        return preferences.getString("userId", null);
    }

    public void setUserId(String userId) {
        editor.putString("userId", userId);
        editor.commit();
    }

    public String getUserName() {
        return preferences.getString("userName", null);
    }

    public void setUserName(String userName) {
        editor.putString("userName", userName);
        editor.commit();
    }

    public String getUserEmail() {
        return preferences.getString("userEmail", null);
    }

    public void setUserEmail(String userEmail) {
        editor.putString("userEmail", userEmail);
        editor.commit();
    }

    public String getCodeAuto() {
        return preferences.getString("codeauto", null);
    }

    public void setCodeAuto(String codeauto) {
        editor.putString("codeauto", codeauto);
        editor.commit();
    }
}
