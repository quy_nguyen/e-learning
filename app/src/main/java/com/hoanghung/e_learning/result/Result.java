package com.hoanghung.e_learning.result;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;
import com.hoanghung.e_learning.test.Question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class Result extends AppCompatActivity {
    ImageView imageBack;
    Animation animation;
    TextView txtTitle, txtNopbai;
    ListView listView;
    Button btnThoat;
    CountDownTimer countDownTimer;
    String URL = "";
    AccountManager accountManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_result);
        mapping();
        setCustomActionBar();
        setResult();
        btnThoat = (Button) findViewById(R.id.button_thoat);
        btnThoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void mapping() {
        accountManager = new AccountManager(getApplicationContext());
    }

    private void setResult() {

        listView = (ListView) findViewById(R.id.list_result);
        ArrayList<Integer> arrayAnswer = new ArrayList<Integer>();
        btnThoat = (Button) findViewById(R.id.button_thoat);
        Intent intent = getIntent();
        Bundle getBundle = intent.getBundleExtra("bundle");
        int TestID = getBundle.getInt("testId");
        int courseContentDataId = getBundle.getInt("courseContentDataId");

        TextView textResult = (TextView) findViewById(R.id.text_result);

        Dialog result = new Dialog(this);
        result.setContentView(R.layout.popup_result);
        result.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView message = (TextView) result.findViewById(R.id.message);
        GifImageView gif = (GifImageView) result.findViewById(R.id.gif);
        GifImageView gif_star = (GifImageView) result.findViewById(R.id.gif_star);

        TextView ok = (TextView) result.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.dismiss();
            }
        });

//        if(Integer.parseInt(getBundle.getString("point")) >= 80){
//            message.setText("Chúc mừng bạn đã hoàn thành xuất sắc bài tập, chúng ta cùng học tiếp nào");
//            gif.setImageResource(R.mipmap.dat);
//            gif_star.setImageResource(R.mipmap.star_drop);
//            gif_star.setVisibility(View.VISIBLE);
//        } else {
//            message.setText("Ôi chưa đạt rồi, mục tiêu 80 điểm, làm lại nào, cố lên");
//            gif.setImageResource(R.mipmap.chuadat);
//            gif_star.setVisibility(View.GONE);
//        }

        countDownTimer = new  CountDownTimer(2500, 1000){
            @Override
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                result.show();
            }
        };

        countDownTimer.start();
    }

    private void setCustomActionBar() {

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar_test);
        getSupportActionBar().setElevation(0);

        txtTitle = (TextView) findViewById(R.id.title_test);
        txtTitle.setText("Kết quả");
//        txtTitle.setText("Magic");

        imageBack = (ImageView) findViewById(R.id.ic_back_test);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animation);
                onBackPressed();
            }
        });

        txtNopbai = (TextView) findViewById(R.id.text_nop_bai);
        txtNopbai.setText("Thoát");
        txtNopbai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        countDownTimer.cancel();
    }
}
