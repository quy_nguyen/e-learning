package com.hoanghung.e_learning.result;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.FuriganaView;
import com.hoanghung.e_learning.model.RelativeRadioGroup;
import com.hoanghung.e_learning.test.Question;

import java.util.ArrayList;
import java.util.List;

public class ResultAdapter extends BaseAdapter {
    List<Submitanswer> submitanswerList;
    ArrayList<Integer> arrayAnswer = new ArrayList<Integer>();
    Context context;
    Bundle getBundle;
    ViewHolder viewHolder = new ViewHolder();

    public ResultAdapter(Context context, Bundle getBundle, List<Submitanswer> submitanswerList, ArrayList<Integer> arrayAnswer) {
        this.submitanswerList = submitanswerList;
        this.context = context;
        this.getBundle = getBundle;
        this.arrayAnswer = arrayAnswer;
    }


    @Override
    public int getCount() {
        return submitanswerList.size();
    }

    @Override
    public Object getItem(int position) {
        return submitanswerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Submitanswer submitanswer = submitanswerList.get(position);

        if (convertView == null) {

        }
        convertView = LayoutInflater.from(context).inflate(R.layout.design_result, parent, false);
        viewHolder.radioGroup = (RelativeRadioGroup) convertView.findViewById(R.id.radio_group);
        viewHolder.textQuestion = (FuriganaView) convertView.findViewById(R.id.text_question);
        viewHolder.correctAnswer = (FuriganaView) convertView.findViewById(R.id.correct_answer);
        viewHolder.txtGiaiThich = (FuriganaView) convertView.findViewById(R.id.giai_thich);

        viewHolder.radioAnswer1 = (RadioButton) convertView.findViewById(R.id.radio_answer1);
        viewHolder.radioAnswer2 = (RadioButton) convertView.findViewById(R.id.radio_answer2);
        viewHolder.radioAnswer3 = (RadioButton) convertView.findViewById(R.id.radio_answer3);
        viewHolder.radioAnswer4 = (RadioButton) convertView.findViewById(R.id.radio_answer4);

        viewHolder.furi1 = (FuriganaView) convertView.findViewById(R.id.furi_1);
        viewHolder.furi2 = (FuriganaView) convertView.findViewById(R.id.furi_2);
        viewHolder.furi3 = (FuriganaView) convertView.findViewById(R.id.furi_3);
        viewHolder.furi4 = (FuriganaView) convertView.findViewById(R.id.furi_4);

        fillResult(submitanswer, position);
        String answer = null;
        int radioId = viewHolder.radioGroup.getCheckedRadioButtonId();
        if(radioId != 0){
             RadioButton radioButton = (RadioButton) convertView.findViewById(radioId);
            if(radioButton.getText().toString() == submitanswerList.get(position).getCorrectanswer()){
                radioButton.setTextColor(Color.parseColor("#079be0"));
                viewHolder.correctAnswer.setVisibility(View.GONE);
                if(Build.VERSION.SDK_INT>=21)
                {

                    ColorStateList colorStateList = new ColorStateList(
                            new int[][]{

                                    new int[]{-android.R.attr.state_enabled}, //disabled
                                    new int[]{android.R.attr.state_enabled} //enabled
                            },
                            new int[] {

                                    Color.BLACK //disabled
                                    ,Color.parseColor("#079be0") //enabled

                            }
                    );

                    radioButton.setButtonTintList(colorStateList);//set the color tint list
                    radioButton.invalidate(); //could not be necessary

                }
            } else {
                radioButton.setTextColor(Color.parseColor("#ff2b2b"));
                if(Build.VERSION.SDK_INT>=21)
                {

                    ColorStateList colorStateList = new ColorStateList(
                            new int[][]{

                                    new int[]{-android.R.attr.state_enabled}, //disabled
                                    new int[]{android.R.attr.state_enabled} //enabled
                            },
                            new int[] {

                                    Color.BLACK //disabled
                                    ,Color.parseColor("#ff2b2b") //enabled

                            }
                    );

                    radioButton.setButtonTintList(colorStateList);//set the color tint list
                    radioButton.invalidate(); //could not be necessary
                }
            }
            answer = radioButton.getText().toString();
        }
        fillTextResult(submitanswer, position, answer);

        return convertView;
    }

    private void fillResult(Submitanswer submitanswer, int position) {

        viewHolder.textQuestion.setText(submitanswer.getQuestion());
        viewHolder.correctAnswer.setText("Đáp án đúng là: " + ((Submitanswer) submitanswerList.get(position)).getCorrectanswer());
        if(submitanswerList.get(position).getFeedback() == null){
            viewHolder.txtGiaiThich.setVisibility(View.GONE);
        }else {
            viewHolder.txtGiaiThich.setVisibility(View.VISIBLE);
            viewHolder.txtGiaiThich.setText("Giải thích: " + ((Submitanswer) submitanswerList.get(position)).getFeedback());
        }
        //Fill Answer

        viewHolder.radioAnswer1.setText(submitanswer.getA());
        viewHolder.radioAnswer2.setText(submitanswer.getB());
        viewHolder.radioAnswer3.setText(submitanswer.getC());
        viewHolder.radioAnswer4.setText(submitanswer.getD());

        //fill Result
        viewHolder.radioGroup.check(getBundle.getInt("A" + position));
//        radioGroup.check(getBundle.getInt("Q"+position));
    }

    private void fillTextResult(Submitanswer submitanswer, int position, String s) {
        viewHolder.furi1.setText(submitanswer.getA());
        viewHolder.furi2.setText(submitanswer.getB());
        viewHolder.furi3.setText(submitanswer.getC());
        viewHolder.furi4.setText(submitanswer.getD());

        if(viewHolder.furi1.getText().equals(s)){
            if(viewHolder.furi1.getText().equals(submitanswer.getCorrectanswer())){
                viewHolder.furi1.setTextColor(Color.parseColor("#079be0"));
            } else {
                viewHolder.furi1.setTextColor(Color.parseColor("#de0b24"));
            }
        }

        if(viewHolder.furi2.getText().equals(s)){
            if(viewHolder.furi2.getText().equals(submitanswer.getCorrectanswer())){
                viewHolder.furi2.setTextColor(Color.parseColor("#079be0"));
            } else {
                viewHolder.furi2.setTextColor(Color.parseColor("#de0b24"));
            }
        }

        if(viewHolder.furi3.getText().equals(s)){
            if(viewHolder.furi3.getText().equals(submitanswer.getCorrectanswer())){
                viewHolder.furi3.setTextColor(Color.parseColor("#079be0"));
            } else {
                viewHolder.furi3.setTextColor(Color.parseColor("#de0b24"));
            }
        }

        if(viewHolder.furi4.getText().equals(s)){
            if(viewHolder.furi4.getText().equals(submitanswer.getCorrectanswer())){
                viewHolder.furi4.setTextColor(Color.parseColor("#079be0"));
            } else {
                viewHolder.furi4.setTextColor(Color.parseColor("#de0b24"));
            }
        }

    }


    static class ViewHolder {

        FuriganaView textQuestion;

        RadioButton radioAnswer1;
        RadioButton radioAnswer2;
        RadioButton radioAnswer3;
        RadioButton radioAnswer4;

        FuriganaView furi1;
        FuriganaView furi2;
        FuriganaView furi3;
        FuriganaView furi4;

        FuriganaView correctAnswer;
        FuriganaView txtGiaiThich;

        RelativeRadioGroup radioGroup;

    }
}
