package com.hoanghung.e_learning.listlesson;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.lesson.Lesson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListLessonAdapter extends RecyclerView.Adapter<ListLessonAdapter.RecyclerViewHolder>{
    Context context;
    int resource;
    List<CourseContent> arrayList;

    public ListLessonAdapter(Context context, int resource,  List<CourseContent> arrayList){
        this.context = context;
        this.resource = resource;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ListLessonAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(resource, parent, false);
        return new ListLessonAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListLessonAdapter.RecyclerViewHolder holder, int position) {
        CourseContent courseContent = arrayList.get(position);
        holder.lessonName.setText(arrayList.get(position).getLessonname());
        holder.lessonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle setBundle = new Bundle();
                setBundle.putString("title", arrayList.get(position).getLessonname());
                setBundle.putInt("id", arrayList.get(position).getId());
                Intent intent = new Intent(context, Lesson.class);
                intent.putExtra("bundle", setBundle);
                context.startActivity(intent);
            }
        });
        holder.lessonStatus.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView lessonName;
        TextView lessonStatus;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            lessonName = (TextView) itemView.findViewById(R.id.text_list_lesson);
            lessonStatus = (TextView) itemView.findViewById(R.id.status_list_lesson);
        }
    }
}

