package com.hoanghung.e_learning.listlesson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CourseContent {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("lessonname")
    @Expose
    private String lessonname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLessonname() {
        return lessonname;
    }

    public void setLessonname(String lessonname) {
        this.lessonname = lessonname;
    }
}
