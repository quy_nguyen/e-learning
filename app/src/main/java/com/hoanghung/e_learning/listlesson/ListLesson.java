package com.hoanghung.e_learning.listlesson;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;

public class ListLesson extends AppCompatActivity {

    TextView txtTitle;
    ImageView imageBack;
    Animation animation;
    RecyclerView listLesson;
    String URL = "";
    Intent getIntent;
    Bundle getBundle;
    int length;
    AccountManager accountManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        accountManager = new AccountManager(getApplicationContext());
        setContentView(R.layout.activity_list_lesson);
        setCustomActionBar();
        setOnClick();
        setListLesson();
    }


    private void setListLesson() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        listLesson = (RecyclerView) findViewById(R.id.list_lesson);
        int id = getBundle.getInt("id");

        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<CourseContent>> call = service.getCourseContent(id);
        call.enqueue(new Callback<APIObject<CourseContent>>() {
            @Override
            public void onResponse(Call<APIObject<CourseContent>> call, retrofit2.Response<APIObject<CourseContent>> response) {
                if(response.body() != null){
                    progressBar.setVisibility(View.GONE);
                    ListLessonAdapter listLessonAdapter = new ListLessonAdapter(ListLesson.this, R.layout.design_list_lesson, response.body().getData() );
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ListLesson.this, RecyclerView.VERTICAL, false);
                    listLesson.addItemDecoration(new DividerItemDecoration(listLesson.getContext(), DividerItemDecoration.VERTICAL));
                    listLesson.setHasFixedSize(true);
                    listLesson.setLayoutManager((linearLayoutManager));
                    listLesson.setAdapter(listLessonAdapter);
                }
            }

            @Override
            public void onFailure(Call<APIObject<CourseContent>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Có lỗi xảy ra", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setCustomActionBar() {
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar_inside_home);
        getSupportActionBar().setElevation(0);

        txtTitle = (TextView) findViewById(R.id.title_inside_home);
        getIntent = getIntent();
        getBundle = getIntent.getBundleExtra("bundle");
        if (getBundle != null) {
            txtTitle.setText(getBundle.getString("title"));
        }
    }

    private void setOnClick() {

        imageBack = (ImageView) findViewById(R.id.ic_back_to_home);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animation);
                onBackPressed();
            }
        });
    }

}
