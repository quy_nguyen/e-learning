package com.hoanghung.e_learning.register;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.login.Login;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;

public class Register extends Activity {

    private Button buttonRegister;
    private ImageView ic_password, ic_confirm;
    private EditText edt_usename, edt_email, edt_password, edt_confirm;
    private ProgressBar loading_register;
    private String URL = "";
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        mapping();
        setOnClick();
    }

    private void setOnClick() {

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                //Find the currently focused view, so we can grab the correct window token from it.
                View view = getCurrentFocus();
                //If no view currently has focus, create a new one, just so we can grab a window token from it
                if (view == null) {
                    view = new View(getApplicationContext());
                }
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mUsename = edt_usename.getText().toString();
                String mEmail = edt_email.getText().toString();
                String mPass = edt_password.getText().toString();
                String mConfirm = edt_confirm.getText().toString();

                if (mUsename.isEmpty() || mEmail.isEmpty() || mPass.isEmpty() || mConfirm.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Hãy nhập đủ thông tin", Toast.LENGTH_SHORT).show();
                } else {
                    loading_register.setVisibility(View.VISIBLE);
                    buttonRegister.setText(null);
                    buttonRegister.setEnabled(false);
                    register(mUsename, mEmail, mPass, mConfirm);
                }
            }
        });

        ic_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_NORMAL);
                    return true;
                }
                return false;
            }
        });

        ic_confirm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    edt_confirm.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    edt_confirm.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_NORMAL);
                    return true;
                }
                return false;
            }
        });
    }

    private void mapping() {
        relativeLayout = (RelativeLayout) findViewById(R.id.layout);
        overridePendingTransition(R.anim.move_in_right, R.anim.move_out_right);
        ic_password = (ImageView) findViewById(R.id.ic_eye_register);
        ic_confirm = (ImageView) findViewById(R.id.ic_eye_confirm);
        edt_password = (EditText) findViewById(R.id.register_password);
        edt_confirm = (EditText) findViewById(R.id.confirm_password);
        edt_usename = (EditText) findViewById(R.id.register_username);
        edt_email = (EditText) findViewById(R.id.register_email);
        buttonRegister = (Button) findViewById(R.id.button_register);
        loading_register = (ProgressBar) findViewById(R.id.loading_register);
        loading_register.setVisibility(View.GONE);
    }

    private void register(final String mUsename, final String mEmail, final String mPass, final String mConfirm) {
        URL = getResources().getString(R.string.sever)+"register";
        buttonRegister = (Button) findViewById(R.id.button_register);
        loading_register = (ProgressBar) findViewById(R.id.loading_register);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                loading_register.setVisibility(View.GONE);
                buttonRegister.setText("Register");
                buttonRegister.setEnabled(true);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String success = "Successfully created user!";
                    final String message = jsonObject.getString("message");
                    if (message.equals(success)) {
                        Intent intent = new Intent(Register.this, Login.class);
                        startActivity(intent);
                    }
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Catch", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading_register.setVisibility(View.GONE);
                buttonRegister.setText("Register");
                buttonRegister.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Lỗi kết nối, vui lòng kiểm tra đường truyền", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", mUsename);
                params.put("email", mEmail);
                params.put("password", mPass);
                params.put("password_confirmation", mConfirm);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
