package com.hoanghung.e_learning.homecontent;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.listlesson.ListLesson;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;

public class HomeContentAdapter extends RecyclerView.Adapter<HomeContentAdapter.RecyclerViewHolder> {
    Context context;
    int resource;
    List<LevelContent> arrayList;
    AccountManager accountManager;
    ProgressBar progressBar;
    Dialog popup;

    public HomeContentAdapter(Context context, int resource, List<LevelContent> arrayList) {
        this.context = context;
        this.resource = resource;
        this.arrayList = arrayList;
        accountManager = new AccountManager(context);
    }

    @NonNull
    @Override
    public HomeContentAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(resource, parent, false);
        return new HomeContentAdapter.RecyclerViewHolder(view);
    }

    private void dialog(int position) {
        popup = new Dialog(context);
        popup.setContentView(R.layout.popup_putstatus);
        popup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView ok = (TextView) popup.findViewById(R.id.ok);
        TextView no = (TextView) popup.findViewById(R.id.no);
        progressBar = (ProgressBar) popup.findViewById(R.id.progress_bar);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putstatus(position);
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        popup.show();
    }

    @Override
    public void onBindViewHolder(@NonNull HomeContentAdapter.RecyclerViewHolder holder, int position) {
        LevelContent levelContent = arrayList.get(position);
        holder.courseName.setText(levelContent.getLevelcourse());
        if (levelContent.getLevelStatus()) {
            holder.courseName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle setBundle = new Bundle();
                    setBundle.putString("title", levelContent.getLevelcourse());
                    setBundle.putInt("id", levelContent.getId());
                    Intent intent = new Intent(context, ListLesson.class);
                    intent.putExtra("bundle", setBundle);
                    context.startActivity(intent);
                }
            });
            holder.courseStatus.setVisibility(View.INVISIBLE);
        } else {
            holder.courseName.setEnabled(false);
            holder.courseName.setTextColor(Color.GRAY);
            holder.courseStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog(position);
                }
            });
        }

    }

    private void putstatus(int position) {

        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject> call =  service.postStatus(accountManager.getUserId(), arrayList.get(position).getId(), 1);
        call.enqueue(new Callback<APIObject>() {
            @Override
            public void onResponse(Call<APIObject> call, retrofit2.Response<APIObject> response) {
                if(response.body() != null){
                    if(response.body().getStatusCode() == 200){
                        popup.dismiss();
                        ((HomeContentActivity) context).getRegisteredCourseID();
                        Toast.makeText(context, "Mua thành công", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<APIObject> call, Throwable t) {
                popup.dismiss();
                Toast.makeText(context, "Mua thất bại", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView courseName;
        TextView courseStatus;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            courseName = (TextView) itemView.findViewById(R.id.text_list_lesson);
            courseStatus = (TextView) itemView.findViewById(R.id.status_list_lesson);
        }
    }
}
