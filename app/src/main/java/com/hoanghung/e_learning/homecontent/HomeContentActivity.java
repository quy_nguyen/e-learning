package com.hoanghung.e_learning.homecontent;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;

public class HomeContentActivity extends AppCompatActivity {

    AccountManager accountManager;
    String URL;
    Integer id;
    RecyclerView listView;
    Intent getIntent;
    Bundle getBundle;

    Animation animationFade;
    ImageView imageBack;
    ArrayList<Integer> registeredCourseID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home_content);
        mapping();
        getRegisteredCourseID();
        setCustomActionBar();
    }

    private void mapping() {
        accountManager = new AccountManager(HomeContentActivity.this);
        getIntent = getIntent();
        getBundle = new Bundle();
        getBundle = getIntent.getBundleExtra("bundle");
    }

    public void getRegisteredCourseID() {
        registeredCourseID = new ArrayList<>();
        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<LevelStatus>> call = service.getLevelStatus(accountManager.getUserId());
        call.enqueue(new Callback<APIObject<LevelStatus>>() {
            @Override
            public void onResponse(Call<APIObject<LevelStatus>> call, retrofit2.Response<APIObject<LevelStatus>> response) {
                if(response.body() != null){
                    List<LevelStatus> levelContentList = response.body().getData();
                    for (int i = 0; i < levelContentList.size(); i++) {
                        registeredCourseID.add(levelContentList.get(i).getId());
                        fillData();
                    }
                } else {
                    fillData();
                }

            }

            @Override
            public void onFailure(Call<APIObject<LevelStatus>> call, Throwable t) {
                fillData();
            }
        });

    }

    private void fillData() {
        id = getBundle.getInt("id");
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        listView = (RecyclerView) findViewById(R.id.list_home_content);

        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<LevelContent>> call = service.getLevelContent(id);
        call.enqueue(new Callback<APIObject<LevelContent>>() {
            @Override
            public void onResponse(Call<APIObject<LevelContent>> call, retrofit2.Response<APIObject<LevelContent>> response) {
                if (response.body().getStatusCode() == 200) {
                    progressBar.setVisibility(View.GONE);
                    LevelContent levelContent;
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        levelContent = response.body().getData().get(i);
                        for (int j = 0; j < registeredCourseID.size(); j++) {
                            if(levelContent.getId() == (registeredCourseID.get(j))){
                                levelContent.setLevelStatus(true);
                            }
                        }
                    }
                    HomeContentAdapter homeContentAdapter = new HomeContentAdapter(HomeContentActivity.this, R.layout.design_list_lesson, response.body().getData());
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeContentActivity.this, RecyclerView.VERTICAL, false);
                    listView.addItemDecoration(new DividerItemDecoration(listView.getContext(), DividerItemDecoration.VERTICAL));
                    listView.setHasFixedSize(true);
                    listView.setLayoutManager((linearLayoutManager));
                    listView.setAdapter(homeContentAdapter);
                }
            }

            @Override
            public void onFailure(Call<APIObject<LevelContent>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.e("onFailure", t.getMessage() + "");
            }
        });

    }

    private void setCustomActionBar() {

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar_inside_home);
        getSupportActionBar().setElevation(0);

        TextView txtTitle = (TextView) findViewById(R.id.title_inside_home);
        getIntent = getIntent();
        txtTitle.setText(getBundle.getString("title"));

        animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);
        imageBack = (ImageView) findViewById(R.id.ic_back_to_home);
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animationFade);
                onBackPressed();
                finish();
            }
        });
    }
}
