package com.hoanghung.e_learning.homecontent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LevelStatus {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("levelcourse")
    @Expose
    private String levelcourse;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLevelcourse() {
        return levelcourse;
    }

    public void setLevelcourse(String levelcourse) {
        this.levelcourse = levelcourse;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
