package com.hoanghung.e_learning.homecontent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LevelContent {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("namecourse")
    @Expose
    private String namecourse;

    @SerializedName("levelcourse")
    @Expose
    private String levelcourse;

    private Boolean levelStatus = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNamecourse() {
        return namecourse;
    }

    public void setNamecourse(String namecourse) {
        this.namecourse = namecourse;
    }

    public String getLevelcourse() {
        return levelcourse;
    }

    public void setLevelcourse(String levelcourse) {
        this.levelcourse = levelcourse;
    }


    public Boolean getLevelStatus() {
        return levelStatus;
    }

    public void setLevelStatus(Boolean levelStatus) {
        this.levelStatus = levelStatus;
    }
}
