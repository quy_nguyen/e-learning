package com.hoanghung.e_learning.lessoncontent;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;

public class LessonContentYoutube extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{
    AccountManager accountManager;
    ListView listTest;
    ImageView imageBack;
    TextView txtTitle;
    String URLvideo = "Không có video nào ở đây cả";
    Bundle getBundle;
    Intent getIntent;
    YouTubePlayerView youTubePlayerView;
    public static final String API_KEY = "AIzaSyCgo1ycN-ATGC7V131-7YqpikWLh8Wb8Vo";
    public static final String VIDEO_ID = "T0sHaz4H9MQ";

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_lesson_youtube);
        mapping();
        setListTest();
        setVideo();
    }

    private void setListTest() {
        int courseContentDataId = getBundle.getInt("courseContentDataId");
        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<TypeOfTest>> call = service.getTypeOfTest(courseContentDataId);
        call.enqueue(new Callback<APIObject<TypeOfTest>>() {
            @Override
            public void onResponse(Call<APIObject<TypeOfTest>> call, retrofit2.Response<APIObject<TypeOfTest>> response) {
                if(response.body() != null){
                    ListTestAdapter listTestAdapter = new ListTestAdapter(LessonContentYoutube.this, R.layout.design_list_test, response.body().getData());
                    listTest.setAdapter(listTestAdapter);
                }
            }

            @Override
            public void onFailure(Call<APIObject<TypeOfTest>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Có lỗi xảy ra", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setVideo() {
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        if (URLvideo.equals("Không có video nào ở đây cả")) {
            youTubePlayerView.setVisibility(View.GONE);
        } else {
            youTubePlayerView.initialize(API_KEY, this);
        }

    }

    private void mapping() {
        getIntent = getIntent();
        if(getIntent.getBundleExtra("bundle") != null){
            getBundle = getIntent.getBundleExtra("bundle");
            URLvideo = getBundle.getString("urlYoutube");
            Log.e("URLvideo", URLvideo);
        }else {
            URLvideo = "Không có video nào ở đây cả";
        }

        accountManager = new AccountManager(getApplicationContext());
        listTest = (ListView) findViewById(R.id.list_test);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");

        txtTitle = (TextView) findViewById(R.id.title_inside_home);
        txtTitle.setText(bundle.getString("title"));

        imageBack = (ImageView) findViewById(R.id.ic_back_to_home);
        Animation animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animationFade);
                onBackPressed();
                finish();
            }
        });
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if(null== youTubePlayer) return;

        // Start buffering
        if (!wasRestored) {
            youTubePlayer.cueVideo(URLvideo);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }

//    @Override
//    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader thumbnailLoader) {
//        youTubeThumbnailLoader = thumbnailLoader;
//        thumbnailLoader.setOnThumbnailLoadedListener(new ThumbnailListener());
//
//        youTubeThumbnailLoader.setVideo("T0sHaz4H9MQ");
//
//        youTubeThumbnailView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(LessonContentYoutube.this, ActivityYoutubeVideo.class);
//                startActivity(intent);
//            }
//        });
//    }
//
//    @Override
//    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
//        String errorMessage =
//                String.format("onInitializationFailure (%1$s)",
//                        youTubeInitializationResult.toString());
//        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
//    }
//
//    private final class ThumbnailListener implements
//            YouTubeThumbnailLoader.OnThumbnailLoadedListener {
//
//        @Override
//        public void onThumbnailLoaded(YouTubeThumbnailView thumbnail, String videoId) {
//
//        }
//
//        @Override
//        public void onThumbnailError(YouTubeThumbnailView thumbnail,
//                                     YouTubeThumbnailLoader.ErrorReason reason) {
//            Toast.makeText(getApplicationContext(),
//                    "onThumbnailError", Toast.LENGTH_SHORT).show();
//        }
//    }
}
