package com.hoanghung.e_learning.lessoncontent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TypeOfTest {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nameoftest")
    @Expose
    private String nameoftest;
    @SerializedName("contentdataid")
    @Expose
    private Integer contentdataid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameoftest() {
        return nameoftest;
    }

    public void setNameoftest(String nameoftest) {
        this.nameoftest = nameoftest;
    }

    public Integer getContentdataid() {
        return contentdataid;
    }

    public void setContentdataid(Integer contentdataid) {
        this.contentdataid = contentdataid;
    }
}
