package com.hoanghung.e_learning.lessoncontent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.test.Test;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ListTestAdapter extends ArrayAdapter<TypeOfTest> {
    private Context context;
    private int resource;
    private List<TypeOfTest> listTestObjectArrayTypeOf;

    public ListTestAdapter(@NonNull Context context, int resource, @NonNull List<TypeOfTest> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.listTestObjectArrayTypeOf = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ListTestAdapter.LessonViewHolder lessonViewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.design_list_test, parent, false);
            lessonViewHolder = new ListTestAdapter.LessonViewHolder();
            lessonViewHolder.txtTest = (TextView) convertView.findViewById(R.id.text_list_test);
            lessonViewHolder.statusTest = (ImageView) convertView.findViewById(R.id.status_list_test);
            convertView.setTag(lessonViewHolder);
        }else {
            lessonViewHolder = (ListTestAdapter.LessonViewHolder) convertView.getTag();
        }

        final TypeOfTest typeOfTest = listTestObjectArrayTypeOf.get(position);

        lessonViewHolder.txtTest.setText(typeOfTest.getNameoftest());
        lessonViewHolder.statusTest.setImageResource(R.drawable.ic_star_nocolor);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Test.class);
                String testName =  typeOfTest.getNameoftest();
                int testId  = typeOfTest.getId();
                int courseContentDataId = typeOfTest.getContentdataid();
                Bundle bundle = new Bundle();
                bundle.putString("testName", testName);
                bundle.putInt("testId", testId);
                bundle.putInt("courseContentDataId", courseContentDataId);
                intent.putExtra("bundle",bundle);
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    static class LessonViewHolder{
        TextView txtTest;
        ImageView statusTest;
    }
}
