package com.hoanghung.e_learning.lessoncontent;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class LessonContentVideo extends AppCompatActivity implements SurfaceHolder.Callback,
        MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControl{

    ImageView imageBack;
    TextView txtTitle;
    Animation animationFade;
    ImageView imageVideoNull;
    TextView txtVideoNull;
    String URLvideo = "Không có video nào ở đây cả";
    String URL = "";
    private int currentPosition, checkedPosition = 0;
    VideoView videoView;
    MediaPlayer mediaPlayer;
    VideoControllerView controller;
    ListView listTest;
    ProgressBar progressBar;
    FrameLayout frameVideo;
    Bundle getBundle;
    Intent getIntent;
    AccountManager accountManager;
    Boolean LANDSCAPE = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_content);
//        EventBus.getDefault().register(this);
        mapping();
        setCustomActionBar(true);
        setVideo();
        setListTest();
    }

    private void mapping() {
        accountManager = new AccountManager(getApplicationContext());
        videoView = (VideoView) findViewById(R.id.video_view);
        txtVideoNull = (TextView) findViewById(R.id.text_video_null);
        imageVideoNull = (ImageView) findViewById(R.id.img_video_null);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        frameVideo = (FrameLayout) findViewById(R.id.frame_video);
        listTest = (ListView) findViewById(R.id.list_test);
    }

    private void setListTest() {
        int courseContentDataId = getBundle.getInt("courseContentDataId");
        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<TypeOfTest>> call = service.getTypeOfTest(courseContentDataId);
        call.enqueue(new Callback<APIObject<TypeOfTest>>() {
            @Override
            public void onResponse(Call<APIObject<TypeOfTest>> call, retrofit2.Response<APIObject<TypeOfTest>> response) {
                if(response.body() != null){
                    ListTestAdapter listTestAdapter = new ListTestAdapter(LessonContentVideo.this, R.layout.design_list_test, response.body().getData());
                    listTest.setAdapter(listTestAdapter);
                }
            }

            @Override
            public void onFailure(Call<APIObject<TypeOfTest>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Có lỗi xảy ra", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setVideo() {

        getIntent = getIntent();
        if(getIntent.getBundleExtra("bundle") != null){
            getBundle = getIntent.getBundleExtra("bundle");
            URLvideo = getBundle.getString("urlVideo");
        }else {
            URLvideo = "Không có video nào ở đây cả";
        }

        if (URLvideo.equals("Không có video nào ở đây cả")) {
            txtVideoNull.setVisibility(View.VISIBLE);
            imageVideoNull.setVisibility(View.VISIBLE);
            frameVideo.setBackgroundColor(Color.WHITE);
            videoView.setVisibility(View.GONE);
            progressBar.setVisibility(View.INVISIBLE);

        } else {
            txtVideoNull.setVisibility(View.INVISIBLE);
            imageVideoNull.setVisibility(View.INVISIBLE);
//            SurfaceHolder surfaceHolder = videoView.getHolder();
//            surfaceHolder.addCallback(this);
            mediaPlayer = new MediaPlayer();
            controller = new VideoControllerView(this);
            videoView.setVideoURI(Uri.parse(URLvideo));
            videoView.setOnPreparedListener(this);
            progressBar.setVisibility(View.VISIBLE);
//            try {
//                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
////                mediaPlayer.setDataSource("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
//                String path = "android.resource://" + getPackageName() + "/" + R.raw.evideo;
//                mediaPlayer.setDataSource("https://media.kantanshosaku.jp/media/public/upload/1.hira-av_1579672961.mp4");
////                mediaPlayer.setDataSource(String.valueOf(Uri.parse(path)));//                mediaPlayer.setOnPreparedListener(this);
//            } catch (IllegalArgumentException e) {
//                e.printStackTrace();
//            } catch (SecurityException e) {
//                e.printStackTrace();
//            } catch (IllegalStateException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            String path = "android.resource://" + getPackageName() + "/" + R.raw.evideo;
        }

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                videoView.stopPlayback();
                Toast.makeText(getApplicationContext(), "Video này có thể không tương thích với thiết bị của bạn", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(controller != null){
                    if(controller.isShowing()){
                        controller.hide();
                    }else {
                        controller.show();
                    }
                }
                return false;
            }
        });
    }

    // Implement SurfaceHolder.Callback
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
//        mediaPlayer.setDisplay(holder);
//        mediaPlayer.prepareAsync();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    // End SurfaceHolder.Callback
    // Implement MediaPlayer.OnPreparedListener
    @Override
    public void onPrepared(MediaPlayer mp) {
        progressBar.setVisibility(View.INVISIBLE);
        controller.setMediaPlayer(this);
        controller.setAnchorView((FrameLayout) findViewById(R.id.frame_video));
        if (currentPosition != 0) {
            videoView.seekTo(currentPosition);
        } else {
            videoView.start();
        }
    }
    // End MediaPlayer.OnPreparedListener

    // Implement VideoMediaController.MediaPlayerControl
    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return videoView.getCurrentPosition();
    }

    @Override
    public int getDuration() {
        return videoView.getDuration();
    }

    @Override
    public boolean isPlaying() {
        return videoView.isPlaying();
    }

    @Override
    public void pause() {
        videoView.pause();
    }

    @Override
    public void seekTo(int i) {
        videoView.seekTo(i);
    }

    @Override
    public void start() {
        videoView.start();
    }

    @Override
    public boolean isFullScreen() {
//        boolean isFullScreen = (getResources().getConfiguration().orientation==2);
//        Log.d(TAG, "isFullScreen: "+isFullScreen);
        if(getResources().getConfiguration().orientation == 2){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void toggleFullScreen() {
        if (getResources().getConfiguration().orientation == 1) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Resources r = getResources();
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 220, r.getDisplayMetrics());
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            frameVideo.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            setCustomActionBar(false);
            controller.hide();
            controller.show();
            LANDSCAPE = true;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            frameVideo.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
            setCustomActionBar(true);
            controller.hide();
            controller.show();
            LANDSCAPE = false;
        }
    }

    private void setCustomActionBar(Boolean aaBar) {

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar_inside_home);
        getSupportActionBar().setElevation(0);
        ActionBar actionBar = getSupportActionBar();
        if(aaBar == true){
            actionBar.show();
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }else {
            actionBar.hide();
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");
        txtTitle = (TextView) findViewById(R.id.title_inside_home);
        txtTitle.setText(bundle.getString("title"));

        imageBack = (ImageView) findViewById(R.id.ic_back_to_home);
        animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animationFade);
                onBackPressed();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(LANDSCAPE == false){
            super.onBackPressed();
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}
