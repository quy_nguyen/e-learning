package com.hoanghung.e_learning.test;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.FuriganaView;
import com.hoanghung.e_learning.model.RelativeRadioGroup;
import com.hoanghung.e_learning.model.RetrofitClient;
import com.hoanghung.e_learning.result.Submitanswer;

import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class TestAdapter extends BaseAdapter {

    List<Question> questionArrayList;
    List<Submitanswer> submitanswerList = new ArrayList<>();
    ArrayList<Integer> arrayRadioAnswer = new ArrayList<Integer>();
    Context context;
    ViewHolder viewHolder = new ViewHolder();
    AccountManager accountManager;
    ListView listView;

    TextView txtNopbai, txtTitle;
    CountDownTimer countDownTimer;
    ProgressBar progressBar;

    ArrayList<Integer> arrayId = new ArrayList<Integer>();
    ArrayList<String> arrayTextAnswer = new ArrayList<String>();

    Boolean NOP_BAI = false;

    public TestAdapter(Context context, List<Question> questions, CountDownTimer countDownTimer) {
        this.questionArrayList = questions;
        this.context = context;
        this.countDownTimer = countDownTimer;
        accountManager = new AccountManager(context);

        mapping();
    }


    private void mapping() {
        listView = (ListView) ((Test) context).findViewById(R.id.list_question);
        progressBar = (ProgressBar) ((Test) context).findViewById(R.id.progress_bar);

        txtNopbai = (TextView) ((Test) context).findViewById(R.id.text_nop_bai);
        txtNopbai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                submitAnswer();
                txtNopbai.setText("Kết Quả");
                progressBar.setVisibility(View.VISIBLE);
                txtNopbai.setVisibility(View.GONE);
                ((Test) context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });

        for (int i = 0; i < questionArrayList.size(); i++) {
            arrayRadioAnswer.add(0);
            arrayId.add(0);
            arrayTextAnswer.add("");
        }

        txtTitle = (TextView) ((Test) context).findViewById(R.id.title_test);
    }


    private void submitAnswer() {
        Question question = questionArrayList.get(0);
        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<Submitanswer>> call = service.submitAnswer(accountManager.getUserId(), question.getCourseContentDataId(), question.getTestID(), arrayId, arrayTextAnswer);
        call.enqueue(new Callback<APIObject<Submitanswer>>() {
            @Override
            public void onResponse(Call<APIObject<Submitanswer>> call, retrofit2.Response<APIObject<Submitanswer>> response) {
                if (response.body() != null) {
                    submitanswerList = response.body().getData();
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        Submitanswer submitanswer = submitanswerList.get(i);
                        String correctAnswer = submitanswer.getCorrectanswer();
                        submitanswer.setCorrectanswer(correctAnswer + ": " +submitanswer.getTextCorrectAnswer(correctAnswer));
                    }
                    result(response.body().getDiem());
                }
            }

            @Override
            public void onFailure(Call<APIObject<Submitanswer>> call, Throwable t) {
                Toast.makeText(context, "Nộp bài thất bại", Toast.LENGTH_SHORT).show();
                ((Test) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                progressBar.setVisibility(View.GONE);
                txtNopbai.setVisibility(View.VISIBLE);
                txtNopbai.setText("Nộp bài");
            }
        });
    }

    private void result(Integer diem) {
        txtNopbai.setVisibility(View.VISIBLE);
        txtNopbai.setText("Điểm: " + diem);
        txtNopbai.setEnabled(false);
        txtTitle.setText("Kết quả");
        listView.invalidateViews();
        progressBar.setVisibility(View.GONE);
        ((Test) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        NOP_BAI = true;

        Dialog result = new Dialog(context);
        result.setContentView(R.layout.popup_result);
        result.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView message = (TextView) result.findViewById(R.id.message);
        TextView textDiem = (TextView) result.findViewById(R.id.text_diem);
        GifImageView gif = (GifImageView) result.findViewById(R.id.gif);
        GifImageView gif_star = (GifImageView) result.findViewById(R.id.gif_star);

        TextView ok = (TextView) result.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.dismiss();
            }
        });

        textDiem.setText(diem + "/100 điểm");

        if (diem >= 80) {
            message.setText("Chúc mừng bạn đã hoàn thành xuất sắc bài tập, chúng ta cùng học tiếp nào");
            gif.setImageResource(R.mipmap.dat);
            gif_star.setImageResource(R.mipmap.star_drop);
            gif_star.setVisibility(View.VISIBLE);
        } else {
            message.setText("Ôi chưa đạt rồi, mục tiêu 80 điểm, làm lại nào, cố lên");
            gif.setImageResource(R.mipmap.chuadat);
            gif_star.setVisibility(View.GONE);
        }
        result.show();
    }

    @Override
    public int getCount() {
        return questionArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return questionArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Question question = questionArrayList.get(position);

        if (convertView == null) {

        }
        convertView = LayoutInflater.from(context).inflate(R.layout.design_test, parent, false);
        viewHolder.radioGroup = (RelativeRadioGroup) convertView.findViewById(R.id.radio_group);
        viewHolder.textQuestion = (FuriganaView) convertView.findViewById(R.id.text_question);

        viewHolder.radioAnswer1 = (RadioButton) convertView.findViewById(R.id.radio_answer1);
        viewHolder.radioAnswer2 = (RadioButton) convertView.findViewById(R.id.radio_answer2);
        viewHolder.radioAnswer3 = (RadioButton) convertView.findViewById(R.id.radio_answer3);
        viewHolder.radioAnswer4 = (RadioButton) convertView.findViewById(R.id.radio_answer4);

        viewHolder.radioAnswer4 = (RadioButton) convertView.findViewById(R.id.radio_answer4);

        viewHolder.furi1 = (FuriganaView) convertView.findViewById(R.id.furi_1);
        viewHolder.furi2 = (FuriganaView) convertView.findViewById(R.id.furi_2);
        viewHolder.furi3 = (FuriganaView) convertView.findViewById(R.id.furi_3);
        viewHolder.furi4 = (FuriganaView) convertView.findViewById(R.id.furi_4);
        viewHolder.textGiaiThich = (FuriganaView) convertView.findViewById(R.id.giai_thich);
        viewHolder.textCorrectAnswer = (FuriganaView) convertView.findViewById(R.id.correct_answer);

        if(arrayRadioAnswer.get(position)!=0){
            viewHolder.radioGroup.check(arrayRadioAnswer.get(position));
        }

        fillQuestion(position, question);
        testing(convertView, position, viewHolder, question);

        int checkedRadioButtonId = viewHolder.radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) convertView.findViewById(checkedRadioButtonId);
        if(NOP_BAI){
            final Submitanswer submitanswer = submitanswerList.get(position);
            viewHolder.textCorrectAnswer.setVisibility(View.VISIBLE);
            viewHolder.textCorrectAnswer.setText("Đáp án đúng là: " + submitanswer.getCorrectanswer());
            if(checkedRadioButtonId != -1) {
                fillResult(submitanswer, radioButton);
            }
        }

        return convertView;

    }

    private void fillResult(Submitanswer submitanswer, RadioButton radioButton) {
        if (submitanswer.getFeedback() == null) {
            viewHolder.textGiaiThich.setVisibility(View.GONE);
        } else {
            viewHolder.textGiaiThich.setText("Giải thích :" + submitanswer.getFeedback());
            viewHolder.textGiaiThich.setVisibility(View.VISIBLE);
        }
        if (submitanswer.getIsCorrect() != null && submitanswer.getIsCorrect()) {
            viewHolder.textCorrectAnswer.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= 21) {
                ColorStateList colorStateList = new ColorStateList(
                        new int[][]{

                                new int[]{-android.R.attr.state_enabled}, //disabled
                                new int[]{android.R.attr.state_enabled} //enabled
                        },
                        new int[]{

                                Color.BLACK //disabled
                                , Color.parseColor("#079be0") //enabled

                        }
                );

                radioButton.setButtonTintList(colorStateList);//set the color tint list
                radioButton.invalidate(); //could not be necessary

            }
            if(viewHolder.furi1.getText().equals(radioButton.getText())) viewHolder.furi1.setTextColor(Color.parseColor("#079be0"));
            if(viewHolder.furi2.getText().equals(radioButton.getText())) viewHolder.furi2.setTextColor(Color.parseColor("#079be0"));
            if(viewHolder.furi3.getText().equals(radioButton.getText())) viewHolder.furi3.setTextColor(Color.parseColor("#079be0"));
            if(viewHolder.furi4.getText().equals(radioButton.getText())) viewHolder.furi4.setTextColor(Color.parseColor("#079be0"));
        } else {
            viewHolder.textCorrectAnswer.setVisibility(View.VISIBLE);

            if (Build.VERSION.SDK_INT >= 21) {
                ColorStateList colorStateList = new ColorStateList(
                        new int[][]{

                                new int[]{-android.R.attr.state_enabled}, //disabled
                                new int[]{android.R.attr.state_enabled} //enabled
                        },
                        new int[]{

                                Color.BLACK //disabled
                                , Color.parseColor("#ff2b2b") //enabled

                        }
                );

                radioButton.setButtonTintList(colorStateList);//set the color tint list
                radioButton.invalidate(); //could not be necessary
            }
            if(viewHolder.furi1.getText().equals(radioButton.getText())) viewHolder.furi1.setTextColor(Color.parseColor("#ff2b2b"));
            if(viewHolder.furi2.getText().equals(radioButton.getText())) viewHolder.furi2.setTextColor(Color.parseColor("#ff2b2b"));
            if(viewHolder.furi3.getText().equals(radioButton.getText())) viewHolder.furi3.setTextColor(Color.parseColor("#ff2b2b"));
            if(viewHolder.furi4.getText().equals(radioButton.getText())) viewHolder.furi4.setTextColor(Color.parseColor("#ff2b2b"));
        }
    }

    private void fillQuestion(int position, Question question) {

        int count = position + 1;
        viewHolder.textQuestion.setText("Câu " + count + ": " + question.getQuestion());

        //Fill Answer
        viewHolder.radioAnswer1.setText("A: " + question.getA());
        viewHolder.radioAnswer2.setText("B: " + question.getB());
        viewHolder.radioAnswer3.setText("C: " + question.getC());
        viewHolder.radioAnswer4.setText("D: " + question.getD());

        viewHolder.furi1.setText("A: " + question.getA());
        viewHolder.furi2.setText("B: " + question.getB());
        viewHolder.furi3.setText("C: " + question.getC());
        viewHolder.furi4.setText("D: " + question.getD());
//
//        if (arrayRadioAnswer.get(position) != 0) {
//            viewHolder.radioGroup.check(arrayRadioAnswer.get(position));
//        }
    }

    private void testing(View convertView, int position, ViewHolder viewHolder, Question question) {
        viewHolder.radioGroup.setOnCheckedChangeListener(new RelativeRadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RelativeRadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) convertView.findViewById(checkedId);
                int idButton = radioButton.getId();

                if (idButton == viewHolder.radioAnswer1.getId()) {
                    arrayTextAnswer.set(position, "a");
                } else if (idButton == viewHolder.radioAnswer2.getId()) {
                    arrayTextAnswer.set(position, "b");
                } else if (idButton == viewHolder.radioAnswer3.getId()) {
                    arrayTextAnswer.set(position, "c");
                } else if (idButton == viewHolder.radioAnswer4.getId()) {
                    arrayTextAnswer.set(position, "d");
                }

                arrayRadioAnswer.set(position, checkedId);
                arrayId.set(position, question.getId());
            }
        });
    }

    static class ViewHolder {
        RelativeRadioGroup radioGroup;
        FuriganaView textQuestion;

        RadioButton radioAnswer1;
        RadioButton radioAnswer2;
        RadioButton radioAnswer3;
        RadioButton radioAnswer4;

        FuriganaView furi1;
        FuriganaView furi2;
        FuriganaView furi3;
        FuriganaView furi4;

        FuriganaView textGiaiThich;
        FuriganaView textCorrectAnswer;
    }
}
