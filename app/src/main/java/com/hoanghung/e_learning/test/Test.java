package com.hoanghung.e_learning.test;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class Test extends AppCompatActivity {

    ImageView imageBack;
    Animation animation;
    TextView txtTitle, txtDiem, txtResult, txtNopbai;
    ListView listView;
    int length;
    Bundle setBundle = new Bundle();
    Bundle getBundle = new Bundle();
    Intent getIntent;
    AccountManager accountManager;
    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_test);
        mapping();
        setCustomActionBar();
        setTest();
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void onEvent(DataOfTest diem) {
//        txtDiem.setVisibility(View.VISIBLE);
        txtResult.setVisibility(View.VISIBLE);
        txtResult.setText(diem.getDiem()+"/100");

//        Toast.makeText(getApplicationContext(), "On Event", Toast.LENGTH_SHORT).show();
    }

    private void mapping() {

        accountManager = new AccountManager(getApplicationContext());
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);

        txtDiem = (TextView) findViewById(R.id.text_diem);

        txtResult = (TextView) findViewById(R.id.text_result);
        getIntent = getIntent();
        getBundle = getIntent.getBundleExtra("bundle");

    }

    private void setTest() {
        Intent getIntent = getIntent();
        getBundle = getIntent.getBundleExtra("bundle");
        int testID = getBundle.getInt("testId");
        int courseContentDataId = getBundle.getInt("courseContentDataId");
        setBundle.putInt("testId", testID);

        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<Question>> call = service.getQuestion(testID);
        call.enqueue(new Callback<APIObject<Question>>() {
            @Override
            public void onResponse(Call<APIObject<Question>> call, retrofit2.Response<APIObject<Question>> response) {
                if(response.body() != null){
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        Question question = response.body().getData().get(i);
                        question.setTestID(testID);
                        question.setCourseContentDataId(courseContentDataId);
                    }
                    length = response.body().getData().size();
                    listView = (ListView) findViewById(R.id.list_question);

                    countDownTimer = new CountDownTimer(100000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            long m = millisUntilFinished / 60000;
                            long s = (millisUntilFinished - m * 60000) / 1000;
                            txtTitle.setText(m + ":" + s);
                        }

                        public void onFinish() {
                            txtNopbai.callOnClick();
                        }
                    };
                    countDownTimer.start();

                    TestAdapter testAdapter = new TestAdapter(Test.this, response.body().getData(), countDownTimer);
                    listView.setAdapter(testAdapter);
                }
            }

            @Override
            public void onFailure(Call<APIObject<Question>> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onStop() {
        countDownTimer.cancel();
        super.onStop();
    }

    private void setCustomActionBar() {

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar_test);
        getSupportActionBar().setElevation(0);

        txtTitle = (TextView) findViewById(R.id.title_test);
        txtNopbai = (TextView) findViewById(R.id.text_nop_bai);

        imageBack = (ImageView) findViewById(R.id.ic_back_test);
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animation);
                onBackPressed();
            }
        });
    }

//    private void submitAnswer() {
//
//        accountManager = new AccountManager(Test.this);
//        getBundle = getIntent.getBundleExtra("bundle");
//        int courseContentDataId = getBundle.getInt("courseContentDataId");
//        int testId = getBundle.getInt("testId");
//        String userId = accountManager.getUserId();
//
//        APIService service = RetrofitClient.getClient().create(APIService.class);
//
//        URL = getResources().getString(R.string.sever) + "submitanswer";
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jsonObjectLessonContent = new JSONObject(response);
//                    if (jsonObjectLessonContent.getString("status_code").equals("200")) {
//                        Log.d("Diem ", jsonObjectLessonContent.getString("diem"));
//                        point = jsonObjectLessonContent.getString("diem");
//                        JSONArray jsonArrayData = jsonObjectLessonContent.getJSONArray("data");
//                        length = jsonArrayData.length();
//                        putResult();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    txtNopbai.setEnabled(true);
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getApplicationContext(), "Đã xảy ra lỗi vui lòng thử lại", Toast.LENGTH_LONG).show();
//                txtNopbai.setEnabled(true);
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("contentdataid",  courseContentDataId+"");
//                params.put("userid", userId);
//                params.put("id", testId+"");
//                for (int i = 0; i < length; i++) {
//                    params.put("idanswer["+i+"]", arrayId.get(i)+"");
//                    params.put("answer["+i+"]", arrayTextAnswer.get(i));
//                }
//
//                return params;
//            }
//
//        };
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

//    private void putResult() {
//        getBundle = getIntent.getBundleExtra("bundle");
//        int courseContentDataId = getBundle.getInt("courseContentDataId");
//
//        Intent intent = new Intent(Test.this, Result.class);
//        for (int i = 0; i < length; i++) {
//            setBundle.putInt("A" + i, arrayAnswer.get(i));
//        }
//
//        Log.d("point", point + "");
//        setBundle.putString("point", point);
//        setBundle.putInt("courseContentDataId", courseContentDataId);
//        intent.putExtra("bundle", setBundle);
//        startActivity(intent);
//        finish();
//    }
}
