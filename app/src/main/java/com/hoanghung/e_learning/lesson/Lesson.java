package com.hoanghung.e_learning.lesson;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hoanghung.e_learning.R;
import com.google.android.material.tabs.TabLayout;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.AccountManager;
import com.hoanghung.e_learning.model.RetrofitClient;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;

public class Lesson extends AppCompatActivity {


    private TabLayout tabLayout;
    private ViewPager viewPager;
    String URL = "";
    int length;
    ViewPagerAdapter adapter;
    ImageView imageBack;
    TextView txtTitle;
    Animation animationFade;
    AccountManager accountManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_lesson);
        mapping();
        setCustomActionBar();
        setContentData();
    }

    private void mapping() {
        accountManager = new AccountManager(getApplicationContext());
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);
    }

    private void setContentData() {
        Intent getIntent = getIntent();
        Bundle getBundle = getIntent.getBundleExtra("bundle");
        int lessonID = getBundle.getInt("id");
        FragmentManager fragmentManager = getSupportFragmentManager();
        adapter = new ViewPagerAdapter(fragmentManager, 0);

        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<ContentData>> call = service.getContentData(lessonID);
        call.enqueue(new Callback<APIObject<ContentData>>() {
            @Override
            public void onResponse(Call<APIObject<ContentData>> call, retrofit2.Response<APIObject<ContentData>> response) {
                if(response.body() != null){
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        ContentData contentData = response.body().getData().get(i);
                        adapter.addFragment(new ListVideoFragment(contentData.getId()), contentData.getNamecoursecontent());
                    }
                    viewPager.setAdapter(adapter);
                    tabLayout.setupWithViewPager(viewPager);
                }
            }

            @Override
            public void onFailure(Call<APIObject<ContentData>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Có lỗi xảy ra", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setCustomActionBar() {

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar_inside_home);
        getSupportActionBar().setElevation(0);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");
        txtTitle = (TextView) findViewById(R.id.title_inside_home);
        txtTitle.setText(bundle.getString("title"));


        animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);
        imageBack = (ImageView) findViewById(R.id.ic_back_to_home);
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animationFade);
                onBackPressed();
                finish();
            }
        });
    }
}
