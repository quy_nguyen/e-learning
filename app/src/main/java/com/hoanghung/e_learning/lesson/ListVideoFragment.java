package com.hoanghung.e_learning.lesson;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.lessoncontent.ListTestAdapter;
import com.hoanghung.e_learning.lessoncontent.TypeOfTest;
import com.hoanghung.e_learning.model.APIObject;
import com.hoanghung.e_learning.model.APIService;
import com.hoanghung.e_learning.model.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;

public class ListVideoFragment extends Fragment {

    private ListView listVideo;
    String URL = "";
    int length;
    int idCourseContent;
    View view;

    public ListVideoFragment(int idCourseContent) {
        this.idCourseContent = idCourseContent;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_video, container, false);
        URL = getResources().getString(R.string.sever) + "getvideodoc";
        getListLesson();
        mapping();
        return view;
    }

    private void mapping() {
        listVideo = (ListView) view.findViewById(R.id.list_video);
    }

    private void getListLesson() {

        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<VideoDoc>> call = service.getVideoDoc(idCourseContent);
        call.enqueue(new Callback<APIObject<VideoDoc>>() {
            @Override
            public void onResponse(Call<APIObject<VideoDoc>> call, retrofit2.Response<APIObject<VideoDoc>> response) {
                if(response.body() != null){
                    try {
                        ListVideoAdapter listVideoAdapter = new ListVideoAdapter(getContext(), R.layout.design_list_video, response.body().getData());
                        listVideo.setAdapter(listVideoAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    setListTest();
                }
            }

            @Override
            public void onFailure(Call<APIObject<VideoDoc>> call, Throwable t) {
                setListTest();
            }
        });
    }

    private void setListTest() {

        TextView textDsbh = (TextView) view.findViewById(R.id.txt_dsbh);

        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<TypeOfTest>> call = service.getTypeOfTest(idCourseContent);
        call.enqueue(new Callback<APIObject<TypeOfTest>>() {
            @Override
            public void onResponse(Call<APIObject<TypeOfTest>> call, retrofit2.Response<APIObject<TypeOfTest>> response) {
                if(response.body() != null){
                    textDsbh.setVisibility(View.VISIBLE);
                    ListTestAdapter listTestAdapter = new ListTestAdapter(getActivity(), R.layout.design_list_test, response.body().getData());
                    listVideo.setAdapter(listTestAdapter);
                }
            }

            @Override
            public void onFailure(Call<APIObject<TypeOfTest>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "Có lỗi xảy ra", Toast.LENGTH_LONG).show();
            }
        });
    }

}
