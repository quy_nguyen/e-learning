package com.hoanghung.e_learning.lesson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("namecoursecontent")
    @Expose
    private String namecoursecontent;
    @SerializedName("coursecontentid")
    @Expose
    private Integer coursecontentid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNamecoursecontent() {
        return namecoursecontent;
    }

    public void setNamecoursecontent(String namecoursecontent) {
        this.namecoursecontent = namecoursecontent;
    }

    public Integer getCoursecontentid() {
        return coursecontentid;
    }

    public void setCoursecontentid(Integer coursecontentid) {
        this.coursecontentid = coursecontentid;
    }
}
