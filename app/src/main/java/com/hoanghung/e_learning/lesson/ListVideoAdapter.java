package com.hoanghung.e_learning.lesson;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hoanghung.e_learning.R;
import com.hoanghung.e_learning.lessoncontent.LessonContentVideo;
import com.hoanghung.e_learning.lessoncontent.LessonContentYoutube;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ListVideoAdapter extends ArrayAdapter<VideoDoc> {

    private Context context;
    private int resource;
    private List<VideoDoc> lessonObjectArrayList;

    public ListVideoAdapter(@NonNull Context context, int resource, @NonNull List<VideoDoc> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.lessonObjectArrayList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LessonViewHolder lessonViewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.design_list_video, parent, false);
            lessonViewHolder = new LessonViewHolder();
            lessonViewHolder.imgVideo = (ImageView) convertView.findViewById(R.id.img_list_video);
            lessonViewHolder.txtVideo = (TextView) convertView.findViewById(R.id.text_list_video);
            lessonViewHolder.imgStatus = (ImageView) convertView.findViewById(R.id.status_list_video);
            convertView.setTag(lessonViewHolder);
        }else {
            lessonViewHolder = (LessonViewHolder) convertView.getTag();
        }

        final VideoDoc videoDoc = lessonObjectArrayList.get(position);

        Picasso.get().load("https://media.kantanshosaku.jp/media/public/image/image.jpg").into(lessonViewHolder.imgVideo);
        lessonViewHolder.txtVideo.setText(videoDoc.getNameVideo());
        lessonViewHolder.imgStatus.setImageResource(R.drawable.ic_star_nocolor);

        convertView.setOnClickListener(v -> {
            Intent intent = new Intent(context, LessonContentYoutube.class);
            if(videoDoc.getType() == 2){
                intent = new Intent(context, LessonContentVideo.class);
            }
            Bundle bundle = new Bundle();
            bundle.putString("title", videoDoc.getNameVideo());
            bundle.putInt("videoID", videoDoc.getId());
            bundle.putString("urlVideo", videoDoc.getUrlvideo());
            bundle.putString("urlYoutube", videoDoc.getUrlyoutube());
            bundle.putInt("courseContentDataId", videoDoc.getCoursecontentdataid());
            intent.putExtra("bundle",bundle);
            context.startActivity(intent);
        });

        return convertView;
    }

    static class LessonViewHolder{
        ImageView imgVideo;
        TextView txtVideo;
        ImageView imgStatus;
    }
}
