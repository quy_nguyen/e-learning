package com.hoanghung.e_learning.lesson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoDoc {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name_video")
    @Expose
    private String nameVideo;

    @SerializedName("namevideo")
    @Expose
    private String namevideo;

    @SerializedName("coursecontentdataid")
    @Expose
    private Integer coursecontentdataid;

    @SerializedName("type")
    @Expose
    private Integer type;

    @SerializedName("urlVideoYoutube")
    @Expose
    private String urlyoutube;

    @SerializedName("urlvideo")
    @Expose
    private String urlvideo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameVideo() {
        return nameVideo;
    }

    public void setNameVideo(String nameVideo) {
        this.nameVideo = nameVideo;
    }

    public String getNamevideo() {
        return namevideo;
    }

    public void setNamevideo(String namevideo) {
        this.namevideo = namevideo;
    }

    public Integer getCoursecontentdataid() {
        return coursecontentdataid;
    }

    public void setCoursecontentdataid(Integer coursecontentdataid) {
        this.coursecontentdataid = coursecontentdataid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUrlyoutube() {
        return urlyoutube;
    }

    public void setUrlyoutube(String urlyoutube) {
        this.urlyoutube = urlyoutube;
    }

    public String getUrlvideo() {
        return urlvideo;
    }

    public void setUrlvideo(String urlvideo) {
        this.urlvideo = urlvideo;
    }
}
